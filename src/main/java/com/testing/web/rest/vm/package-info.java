/**
 * View Models used by Spring MVC REST controllers.
 */
package com.testing.web.rest.vm;
